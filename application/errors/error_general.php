<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Error 404 Page Not Found</title>
		
		<style type="text/css">
		body {
			background-color: #c9c9f1;
		}
		.container {
			width: 1024px;
			height: 300px;
			margin: auto;
			margin-top: 200px;
			position: relative;
			//background-color: red;
			text-align: center;
		}
		h1 {
			color: #fff;
			font-size: 80px;
			font-weight: bold;
		}
		h1:hover{
			color: #151552;
		}
		code {
			font-size: 30px;
			font-weight: bold;
			color: #8686e0;
		}
		a {
			text-decoration: none;
			color: #3131be;
		}
		a:hover{
			color: #fff;
		}
		</style>
	</head>

	<body>
		<div class="container">
			<h1>Oops ! <?php echo $heading; ?></h1>
			<code>
				Silahkan hubungi administrator atau developer <a href="http://amigahost.com/" target="_blank">Amigahost</a>. <?php echo $message; ?>
			<br><a href="http://bappeda.solokkota.go.id/">Kembali kehalaman depan</a>
			</code>
		</div>


	</body>
</html>